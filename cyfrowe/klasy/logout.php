<?php

    $_SESSION['zalogowany'] = FALSE;
    unset($_SESSION['userMail']);
    $_SESSION = array();
    session_unset();
    session_destroy();
    if (session_status() == PHP_SESSION_ACTIVE) {
        session_reset();
        session_destroy();
    }
    
    header('Location: ../index.php');