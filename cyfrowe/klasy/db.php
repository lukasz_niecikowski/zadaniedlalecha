<?php
$dsn = "mysql:host=localhost;dbname=test;charset=utf8mb4";
$options = [
    PDO::ATTR_EMULATE_PREPARES   => false, // turn off emulation mode for "real" prepared statements
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION, //turn on errors in the form of exceptions
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC, //make the default fetch be an associative array
];


try {
        $pdo = new PDO($dsn, "root", "", $options);
        //echo "polaczono z baza danuch";
    } catch( PDOException $e ) {
        echo 'ERROR: ' . $e->getMessage();
    }
?>