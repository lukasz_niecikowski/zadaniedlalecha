<?php

    require_once ('db.php');
    
    $testMail = '/^[a-zA-Z0-9.\-_]+\@[a-zA-Z0-9\-.]+\.[a-zA-Z]+$/';
    $testLogin = '/^[a-ząęółśżźćńA-ZŁŚ0-9]+$/';
    $testImie = '/^[A-ZŁŚ]+[a-ząęółśżźćń]+$/';
    $testNazwisko = '/^[A-ZŁŚ]{1}[a-ząęółśżźćń]+$/';
    $testTelefon = '/^(\+48)?[0-9]{9}$/';

    if(isset($_POST['rejestruj_submit'])){

    if ( session_status() === PHP_SESSION_DISABLED ){
        session_start();
    }
    
    $_SESSION['imie'] = (ucfirst($_POST['imie']));
    $_SESSION['nazwisko'] = (ucfirst($_POST['nazwisko']));
    $_SESSION['mail'] = ($_POST['mail']);
    $_SESSION['tel'] = ($_POST['tel']);
    $_SESSION['dataur'] = ($_POST['dataur']);
    $_SESSION['haslo'] = ($_POST['haslo']);
    $_SESSION['haslo1'] = ($_POST['haslo1']);
    
    if(!(preg_match($testImie,$_SESSION["imie"])) && isset($_SESSION['imie'])){
        echo "bledne imie ";
    }else{
        echo"imie <br/>";
        if(!(preg_match($testNazwisko,$_SESSION["nazwisko"])) && isset($_SESSION['nazwisko'])){
            echo "bledne nazwisko ";
        }else{
            echo"nazwisko <br/>";
            if(!(preg_match($testMail,$_SESSION["mail"])) && isset($_SESSION['mail'])){
                echo "bledny mail ";
            }else{
                echo"mail <br/>";
                if(!(preg_match($testTelefon,$_SESSION["tel"])) && isset($_SESSION['tel'])){
                    echo "bledny numer telefonu ";
                }else{
                    echo"tel <br/>";
                    if(!isset($_SESSION['dataur'])){
                        echo "bledna data urodzenia ";
                    }else{
                        echo"data <br/>";
                        if(!($_SESSION["haslo"] == $_SESSION["haslo1"]) && isset($_SESSION['haslo'])){
                            echo "bledne haslo ";
                        }else{
                            echo"haslo <br/>";
                            $stmt = $pdo->prepare("INSERT INTO uzytkownik (Id, Imie, Nazwisko, Email, Telefon, Data_ur, Haslo) VALUES (?,?,?,?,?,?,?)");
                            $stmt->execute(['', $_SESSION['imie'], $_SESSION['nazwisko'], $_SESSION['mail'], $_SESSION['tel'], $_SESSION['dataur'], $_SESSION['haslo']]);
                            $stmt = null;
                            echo "wysłano";

                            $_SESSION = array();
                            session_unset();
                        }
                    }
                
                }
            }
        }
    }
 }