<?php
    require_once ('klasy/login.php');
    require_once ('klasy/register.php');
    require_once ('klasy/wiadomosc.php');
    if ( session_status() === PHP_SESSION_DISABLED ){
        session_start();
    }
    
    if(isset($_GET['zalogowany']) && isset($_GET['Id'])){
        $zalogowany = $_GET['zalogowany'];
        $_SESSION['zalogowany'] = $_GET['zalogowany'];
        $_SESSION['userId'] = $_GET['Id'];
    }else {
        $zalogowany = "";
        $_SESSION['zalogowany'] = "";
        $_SESSION['userId'] = "";
    }
    if(isset($_GET['operacja'])){
        $_SESSION['operacja'] = $_GET['operacja'];
    }else{
        $_SESSION['operacja'] = "";
    }
    
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
       <!-- <link rel="stylesheet" type="text/css" href="style.css">-->
        <title>zadanko</title>
    </head>
    <body>
        <div id="header"><a href=<?php echo ("index.php?Id=".$_SESSION['userId']."&zalogowany=".$_SESSION['zalogowany']);?>><h1>Zadanie</h1></a></div>
        <div id="menu">
            <div id="log_usr">
                 <?php 
                if(isset($zalogowany) && !empty($zalogowany)){
                    
                    echo '<li><a href="klasy/logout.php">wyloguj</a></li>';
                }else{
                    echo "<li><a href=reglog.php?Id=".$_SESSION['userId']."&zalogowany=".$_SESSION['zalogowany'].">Zaloguj</a></li>";
                    echo "<li><a href=reglog.php?Id=".$_SESSION['userId']."&zalogowany=".$_SESSION['zalogowany'].">Zarejestruj</a></li>";
                }
                ?><br/><br/>
            </div>
            <div id="kontakty">
                <?php
                $sql = 'SELECT Imie, Nazwisko, Email FROM uzytkownik ORDER BY Imie';
                foreach ($pdo->query($sql) as $row) {
                    echo "<a href='opis.php?Imie=".$row['Imie']."&Nazwisko=".$row['Nazwisko']."&Zalogowany=".$_SESSION['zalogowany']."'>".$row['Imie'] . " | ";
                    echo $row['Nazwisko'] . " | ";
                    echo $row['Email'] . "</a><br/>";
                }
                echo '<br/><hr/<br/>';
                ?>
            </div>
        </div>
        <div id="glowny">
            
            <?php
                if(isset($zalogowany) && !empty($zalogowany)){
                    echo '<div id="pisanie">';
                    echo '<form name="wiadomosc" method="post" action="index.php?Id='.$_SESSION['userId'].'&zalogowany='.$_SESSION['zalogowany'].'">';
                    if($_SESSION['operacja'] == "edytuj"){
                        echo "<h3>Edycja rekordu</h3>";
                        echo '<input type="hidden" name="wartosc" value="'.$_GET['idw'].'">';
                    }
                    echo '<textarea name="wiadomosc"></textarea></br>';
                    echo '<input type="reset" name="resetuj">';
                    echo '<input type="submit" name="wyslij" value="wyslij">';
                    echo '</form><br/><br/>';
                    echo '</div>';
                }
            ?>
            
            <div id="czytanie">
            <?php
                if(isset($zalogowany) && !empty($zalogowany)){
                    require_once ('klasy/wiadomosci.php');
                    if($_SESSION['operacja'] == "usun"){
                        
                        $stmt = $pdo->prepare("DELETE FROM wiadomosci WHERE Id=?");
                        $stmt->execute([ $_GET['idw']]);
                        $stmt = null;
                    }else{
                        echo "";
                    }
                }else{
                    echo '<h1>Zaloguj się by czytać lub pisać wiadomości</h1>';
                }
             ?>
            </div>
        </div>
    </body>
</html>
